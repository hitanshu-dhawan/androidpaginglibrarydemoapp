package com.hitanshudhawan.paginglibraryexample

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.hitanshudhawan.paginglibraryexample.network.Repository
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    // https://www.youtube.com/playlist?list=PLk7v1Z2rk4hjCQw1RVoYPRdeIzwdz5_Fi

    private val recyclerViewAdapter by lazy { SearchRepositoriesAdapter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val searchRepositoriesViewModel = ViewModelProviders.of(this).get(SearchRepositoriesViewModel::class.java)
        button.setOnClickListener {
            if (edit_text.text.isNotEmpty()) {
                searchRepositoriesViewModel.searchRepositories(edit_text.text.trim().toString())
                searchRepositoriesViewModel.searchRepositoriesPagedList?.removeObservers(this)
                searchRepositoriesViewModel.searchRepositoriesPagedList?.observe(this, Observer<PagedList<Repository?>> {
                    recyclerViewAdapter.submitList(it)
                })
            }
        }

        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = recyclerViewAdapter
    }
}
