package com.hitanshudhawan.paginglibraryexample

import android.arch.paging.PagedListAdapter
import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.hitanshudhawan.paginglibraryexample.network.Repository

class SearchRepositoriesAdapter(val context: Context) : PagedListAdapter<Repository?, SearchRepositoriesAdapter.ViewHolder>(DIFF_CALLBACK) {

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Repository?>() {
            override fun areItemsTheSame(oldRepository: Repository, newRepository: Repository) = (oldRepository.id == newRepository.id)
            override fun areContentsTheSame(oldRepository: Repository, newRepository: Repository) = (oldRepository == newRepository)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_search_repository, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repository = getItem(position)

        Glide.with(context.applicationContext).load(repository?.owner?.avatarUrl).into(holder.image)
        holder.name.text = repository?.fullName
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.findViewById<ImageView>(R.id.item_search_repository_image_view)!!
        val name = itemView.findViewById<TextView>(R.id.item_search_repository_text_view)!!
    }
}