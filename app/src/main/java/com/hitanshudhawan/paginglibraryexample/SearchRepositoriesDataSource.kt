package com.hitanshudhawan.paginglibraryexample

import android.arch.paging.PageKeyedDataSource
import com.hitanshudhawan.paginglibraryexample.network.ApiClient
import com.hitanshudhawan.paginglibraryexample.network.ApiInterface
import com.hitanshudhawan.paginglibraryexample.network.Repository
import com.hitanshudhawan.paginglibraryexample.network.SearchRepositoriesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchRepositoriesDataSource(val query: String) : PageKeyedDataSource<Int, Repository?>() {

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Repository?>) {
        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        apiService.searchRepositories(query, 1, 20).enqueue(object : Callback<SearchRepositoriesResponse?> {

            override fun onResponse(call: Call<SearchRepositoriesResponse?>?, response: Response<SearchRepositoriesResponse?>?) {
                if (response?.isSuccessful!!) {
                    val repositories = response.body()?.repositories
                    if (repositories != null) callback.onResult(repositories, null, 2)
                }
            }

            override fun onFailure(call: Call<SearchRepositoriesResponse?>?, t: Throwable?) {
                // ...
            }

        })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Repository?>) {
        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        apiService.searchRepositories(query, params.key, 20).enqueue(object : Callback<SearchRepositoriesResponse?> {

            override fun onResponse(call: Call<SearchRepositoriesResponse?>?, response: Response<SearchRepositoriesResponse?>?) {
                if (response?.isSuccessful!!) {
                    val repositories = response.body()?.repositories
                    if (repositories != null) callback.onResult(repositories, if (params.key == 1) null else params.key - 1)
                }
            }

            override fun onFailure(call: Call<SearchRepositoriesResponse?>?, t: Throwable?) {
                // ...
            }

        })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Repository?>) {
        val apiService = ApiClient.getClient().create(ApiInterface::class.java)
        apiService.searchRepositories(query, params.key, 20).enqueue(object : Callback<SearchRepositoriesResponse?> {

            override fun onResponse(call: Call<SearchRepositoriesResponse?>?, response: Response<SearchRepositoriesResponse?>?) {
                if (response?.isSuccessful!!) {
                    val repositories = response.body()?.repositories
                    if (repositories != null) callback.onResult(repositories, if (repositories.isEmpty()) null else params.key + 1)
                }
            }

            override fun onFailure(call: Call<SearchRepositoriesResponse?>?, t: Throwable?) {
                // ...
            }

        })
    }
}