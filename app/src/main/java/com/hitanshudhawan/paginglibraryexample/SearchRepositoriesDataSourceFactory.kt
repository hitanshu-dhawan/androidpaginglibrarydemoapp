package com.hitanshudhawan.paginglibraryexample

import android.arch.paging.DataSource
import com.hitanshudhawan.paginglibraryexample.network.Repository


class SearchRepositoriesDataSourceFactory(val query: String) : DataSource.Factory<Int, Repository?>() {

    override fun create(): DataSource<Int, Repository?> {
        return SearchRepositoriesDataSource(query)
    }


}