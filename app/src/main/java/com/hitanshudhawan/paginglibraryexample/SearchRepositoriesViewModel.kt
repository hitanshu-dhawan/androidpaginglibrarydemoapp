package com.hitanshudhawan.paginglibraryexample

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.hitanshudhawan.paginglibraryexample.network.Repository

class SearchRepositoriesViewModel : ViewModel() {

    var searchRepositoriesPagedList : LiveData<PagedList<Repository?>>? = null

    fun searchRepositories(query: String) {
        val dataSourceFactory = SearchRepositoriesDataSourceFactory(query)
        val config = PagedList.Config.Builder()
                .setPageSize(20)
                .setInitialLoadSizeHint(5)
                .setPrefetchDistance(10)
                .setEnablePlaceholders(false)
                .build()
        searchRepositoriesPagedList = LivePagedListBuilder(dataSourceFactory, config).build()
    }
}