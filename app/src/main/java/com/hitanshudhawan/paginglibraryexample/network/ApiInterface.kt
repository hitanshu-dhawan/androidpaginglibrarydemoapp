package com.hitanshudhawan.paginglibraryexample.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("search/repositories")
    fun searchRepositories(
            @Query("q") query: String,
            @Query("page") page: Int,
            @Query("per_page") perPage: Int
    ): Call<SearchRepositoriesResponse>

}