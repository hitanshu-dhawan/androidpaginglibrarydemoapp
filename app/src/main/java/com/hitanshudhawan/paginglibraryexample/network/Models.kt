package com.hitanshudhawan.paginglibraryexample.network

import com.google.gson.annotations.SerializedName

data class SearchRepositoriesResponse(
        @SerializedName("items")
        val repositories: List<Repository?>?
)

data class Repository(
        @SerializedName("id")
        val id: Long?,
        @SerializedName("full_name")
        val fullName: String?,
        @SerializedName("owner")
        val owner: Owner?
)

data class Owner(
        @SerializedName("avatar_url")
        val avatarUrl: String?
)